;*************************************************
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"

begin
;************************************************
; Create pointer to file and read the parameters.
;************************************************
;************************************************
;22.5N-40N (45-52); 110W(250)-90W(270) (100-108)
   in    = addfile("SPEI_Phi.W.TdT.nc","r") 
   ; extract the variables from central Texas
   lat     = in->lat(45:52)
   lon     = in->lon(100:108)
   time	   = in->time
   spei    = in->SPEI_Jun2Aug(:,45:52,100:108)
   phi     = in->phi500_Mar2May(:,45:52,100:108)
   W	   = in->W_Sep2May(:,45:52,100:108)
   CIN = in->t700_td_Mar2May(:,45:52,100:108)
   
;****************detrend************************
phiDtrend = dtrend_msg_n (time,phi,True,True,0)
print("phiDtrend slope="+phiDtrend@slope) ;phiDtrend@y_intercept
WDtrend   = dtrend_msg_n (time,W,True,True,0)
print("WDtrend slope="+WDtrend@slope)
CINDtrend       = dtrend_msg_n (time,CIN,True,True,0)
print("CINDtrend slope="+CINDtrend@slope)

;************************************************
; Create x and calculate the regression coefficient.
; Note regline works on one dimensional arrays.   
;************************************************
rc1=new((/dimsizes(lat),dimsizes(lon)/),typeof(spei))
rc2=new((/dimsizes(lat),dimsizes(lon)/),typeof(spei))
rc3=new((/dimsizes(lat),dimsizes(lon)/),typeof(spei))
do i=0,dimsizes(lat)-1
	do j= 0, dimsizes(lon)-1
	rc1(i,j)  = regline(phiDtrend(:,i,j),spei(:,i,j))
	rc2(i,j)  = regline(WDtrend(:,i,j),spei(:,i,j))
	rc3(i,j)  = regline(CINDtrend(:,i,j),spei(:,i,j))
	end do
end do
;************************************************
; Create an array to hold both the original data
; and the calculated regression line.
;************************************************
 data1      = new ( (/2,dimsizes(time),dimsizes(lat),dimsizes(lon)/), typeof(spei))
 data2      = new ( (/2,dimsizes(time),dimsizes(lat),dimsizes(lon)/), typeof(spei))
 data3      = new ( (/2,dimsizes(time),dimsizes(lat),dimsizes(lon)/), typeof(spei))
 data1(0,:,:,:) = spei
 data2(0,:,:,:) = spei	 
 data3(0,:,:,:) = spei
; y = mx+b 
; m is the slope:       rc      returned from regline
; b is the y intercept: rc@yave attribute of rc returned from regline
do i=0,dimsizes(lat)-1
	do j= 0, dimsizes(lon)-1
	rc1_temp=rc1(i,j) 	;copyatt(rc1_temp,rc1(i,j))
	rc2_temp=rc2(i,j) 	;copyatt(rc2_temp,rc2(i,j))
	rc3_temp=rc3(i,j) 	;copyatt(rc3_temp,rc3(i,j))
	
	data1(1,:,i,j) = rc1(i,j)*(phiDtrend(:,i,j)-rc1_temp@xave) +rc1_temp@yave
	data2(1,:,i,j) = rc2(i,j)*(WDtrend(:,i,j)-rc2_temp@xave) + rc2_temp@yave
	data3(1,:,i,j) = rc3(i,j)*(CINDtrend(:,i,j)-rc3_temp@xave) + rc3_temp@yave
	end do
end do
;************************************************


; create plots
;************************************************
  
	res                     = True                   ; plot mods desired
	res@gsnDraw  = False                          ; don't draw
    res@gsnFrame = False                          ; don't advance frame
	res@gsnMaximize         = True                   ; maximize plot in frame
	res@xyMarkLineModes     = (/"Markers","Lines"/)  ; choose which have markers
	res@xyMarkers           = 16                     ; choose type of marker 
	res@xyMarkerColor       = "red"                  ; Marker color
	res@xyMarkerSizeF       = 0.005                  ; Marker size (default 0.01)
	res@xyDashPatterns      = 1                      ; solid line 
	res@xyLineThicknesses   = (/1,2/)                ; set second line to 2
	
	
	
	
    ;resP@gsnPanelLabelBar       = True  ;False;              ; add common colorbar
    ;resP@lbLabelFontHeightF     = 0.008               ; make labels smaller
	;resP@lbBottomMarginF 		= 0.15

do i=0,dimsizes(lat)-1
	do j= 0,dimsizes(lon)-1
	fon		= "SeparatePlot/SPEI_Phi.W.TdT"+lat(i)+"_"+lon(j)

	
   ;wks = gsn_open_wks("ps","panel")               ; open a ps file
   wks		= gsn_open_wks("eps",fon)
   plot = new(3,graphic)                          ; create a plot array
   plot(0)  =  gsn_csm_xy(wks,phiDtrend(:,i,j),data1(:,:,i,j),res)        ; create plot
   plot(1)  =  gsn_csm_xy(wks,WDtrend(:,i,j),data2(:,:,i,j),res)        ; create plot
   plot(2)  =  gsn_csm_xy(wks,CINDtrend(:,i,j),data3(:,:,i,j),res)        ; create plot  
   
 ;***********************create panel********************************
	resP                        = True                ; modify the panel plot
	;resP@gsnFrame 				= False 			  ; don't advance panel plot
	resP@txString               = lat(i)+"N "+(360-lon(j))+"W"
	resP@lbLabelFontHeightF     = 0.008               ; make labels smaller
	resP@lbBottomMarginF 		= 0.15
	gsn_panel(wks,plot,(/1,3/),resP)                  ; now draw as one plot
  
  	delete(wks) ;if use delete instead of frame, system("rm -rf "+fon+".eps")  is not necessary 	;frame(wks)
	system("convert -density 500 -trim -rotate -90 "+fon+".eps "+fon+".jpg")
	print("processing ... "+fon)
	system("rm -rf "+fon+".eps") 
  	end do
end do
end